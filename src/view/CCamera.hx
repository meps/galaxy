package view;

import openfl.display.InteractiveObject;
import openfl.events.EventDispatcher;
import openfl.events.MouseEvent;
import openfl.external.ExternalInterface;

class CCamera extends EventDispatcher {
	
	public function new() {
		super(this);
		init();
	}
	
	public var offsetX(default, null):Float = 0;
	public var offsetXMin(default, null):Float = -2;
	public var offsetXMax(default, null):Float = 2;
	
	public var offsetY(default, null):Float = 0;
	public var offsetYMin(default, null):Float = -2;
	public var offsetYMax(default, null):Float = 2;
	
	public var zoom(default, null):Float = 1;
	public var zoomMin(default, null):Float = 0.5;
	public var zoomMax(default, null):Float = 15.0;
	
	public function listen(aSource:InteractiveObject) {
		aSource.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		aSource.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		aSource.addEventListener(MouseEvent.MOUSE_OUT, onMouseUp);
		aSource.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		aSource.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
	}
	
	public function append(aTarget:ICameraTarget) {
		aTarget.place(offsetX, offsetY, zoom);
		if (mTargets.indexOf(aTarget) < 0)
			mTargets.push(aTarget);
	}
	
	public function update(aTime:Float) {
		var redraw = mZoomDirty || mOffsetDirty;
		
		if (mZoomDirty) {
			// пересчитать масштабирование
			var deltaZoom = mZoomTarget - zoom;
			if (-ZOOM_EPSILON < deltaZoom && deltaZoom < ZOOM_EPSILON) {
				mZoomDirty = false;
				zoom = mZoomTarget;
			} else {
				zoom += deltaZoom * ZOOM_EASING;
			}
		}
		
		if (mOffsetDirty) {
			// пересчитать смещение
			var deltaX = mOffsetXTarget - offsetX;
			var deltaY = mOffsetYTarget - offsetY;
			if (-OFFSET_EPSILON < deltaX && deltaX < OFFSET_EPSILON ||
				-OFFSET_EPSILON < deltaY && deltaY < OFFSET_EPSILON) {
				mOffsetDirty = false;
				offsetX = mOffsetXTarget;
				offsetY = mOffsetYTarget;
			} else {
				offsetX += deltaX * OFFSET_EASING;
				offsetY += deltaY * OFFSET_EASING;
			}
		}
		
		if (redraw)
			for (target in mTargets)
				target.place(offsetX, offsetY, zoom);
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	function init() {
		mTargets = new Array();
		if (ExternalInterface.available)
			ExternalInterface.addCallback("jsWheel", onWheelExternal);
	}
	
	function onWheelExternal(aDelta:Float) {
		if (aDelta > 0)
			updateZoom(zoom * ZOOM_RATIO);
		else if (aDelta < 0)
			updateZoom(zoom / ZOOM_RATIO);
	}
	
	function onMouseWheel(aEvent:MouseEvent) {
		if (aEvent.delta > 0)
			updateZoom(zoom * ZOOM_RATIO);
		else if (aEvent.delta < 0)
			updateZoom(zoom / ZOOM_RATIO);
	}
	
	function updateZoom(aValue:Float) {
		if (aValue > zoomMax)
			aValue = zoomMax;
		else if (aValue < zoomMin)
			aValue = zoomMin;
		
		if (aValue == mZoomTarget)
			return;
		
		mZoomTarget = aValue;
		mZoomDirty = true;
	}
	
	function onMouseMove(aEvent:MouseEvent) {
		if (!mMouseDown)
			return;
		
		var deltaX = aEvent.stageX - mMouseLastX;
		var deltaY = aEvent.stageY - mMouseLastY;
		updateOffset(mOffsetXTarget + deltaX / OFFSET_SCALE, mOffsetYTarget + deltaY / OFFSET_SCALE);
		mMouseLastX += deltaX;
		mMouseLastY += deltaY;
	}
	
	function onMouseUp(aEvent:MouseEvent) {
		mMouseDown = false;
	}
	
	function onMouseDown(aEvent:MouseEvent) {
		mMouseDown = true;
		mMouseLastX = aEvent.stageX;
		mMouseLastY = aEvent.stageY;
	}
	
	function updateOffset(aOffsetX:Float, aOffsetY:Float) {
		if (aOffsetX < offsetXMin)
			aOffsetX = offsetXMin;
		else if (aOffsetX > offsetXMax)
			aOffsetX = offsetXMax;
		
		if (aOffsetY < offsetYMin)
			aOffsetY = offsetYMin;
		else if (aOffsetY > offsetYMax)
			aOffsetY = offsetYMax;
		
		if (aOffsetX == mOffsetXTarget && aOffsetY == mOffsetYTarget)
			return;
		
		mOffsetXTarget = aOffsetX;
		mOffsetYTarget = aOffsetY;
		
		mOffsetDirty = true;
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	var mTargets:Array<ICameraTarget>;
	var mMouseDown:Bool = false;
	var mMouseLastX:Float;
	var mMouseLastY:Float;
	
	var mZoomTarget:Float = 1;
	var mZoomDirty:Bool = false;
	
	var mOffsetXTarget:Float = 0;
	var mOffsetYTarget:Float = 0;
	var mOffsetDirty:Bool = false;
	
	static inline var ZOOM_RATIO = 1.5;
	static inline var ZOOM_EPSILON = 0.003;
	static inline var ZOOM_EASING = 0.2;
	
	static inline var OFFSET_EPSILON = 0.0001;
	static inline var OFFSET_EASING =0.4;
	static inline var OFFSET_SCALE = 400; // TODO перенести привязку к масштабу во внешние обработчики
	
}