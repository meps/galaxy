package view;

import haxe.ui.components.Button;
import haxe.ui.components.CheckBox;
import haxe.ui.components.DropDown;
import haxe.ui.components.Label;
import haxe.ui.components.TextField;
import haxe.ui.containers.Box;
import haxe.ui.containers.ListView;
import haxe.ui.containers.VBox;
import haxe.ui.core.Component;
import haxe.ui.core.MouseEvent;
import haxe.ui.core.Screen;
import haxe.ui.core.UIEvent;
import haxe.ui.macros.ComponentMacros;
import model.CSettingsModel;
import model.CStar.EStarType;
import openfl.display.DisplayObject;
import openfl.display.Sprite;
import openfl.events.Event;

class CSettingsView {
	
	public function new() {
		init();
	}
	
	public var container(default, null):Component;
	
	public function bind(aModel:CSettingsModel) {
		mModel = aModel;
		mModel.addEventListener(Event.CHANGE, onModel);
		onModel();
	}
	
	public function resize(aWidth:Int, aHeight:Int) {
		container.width = aWidth;
		container.height = aHeight;
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	function init() {
		container = ComponentMacros.buildComponent("assets/ui/settings.xml");
		
		mInputStars = container.findComponent("stars-value", TextField);
		mInputStars.restrictChars = "0-9";
		mInputStars.registerEvent(UIEvent.CHANGE, function(aEvent:UIEvent) {
			mModel.stars = Std.parseInt(mInputStars.value);
		});
		
		var buttonAdd1K = container.findComponent("stars-add-1k", Button);
		buttonAdd1K.registerEvent(MouseEvent.CLICK, function(aEvent:MouseEvent) {
			mModel.stars += 1000;
		});
		
		var buttonAdd10K = container.findComponent("stars-add-10k", Button);
		buttonAdd10K.registerEvent(MouseEvent.CLICK, function(aEvent:MouseEvent) {
			mModel.stars += 10000;
		});
	
		mInputArms = container.findComponent("arms-value", TextField);
		mInputArms.restrictChars = "0-9";
		mInputArms.registerEvent(UIEvent.CHANGE, function(aEvent:UIEvent) {
			mModel.arms = Std.parseInt(mInputArms.value);
		});
		
		mInputSpin = container.findComponent("spin-value", TextField);
		mInputSpin.restrictChars = "-0-9";
		mInputSpin.registerEvent(UIEvent.CHANGE, function(aEvent:UIEvent) {
			mModel.spin = Std.parseInt(mInputSpin.value);
		});
		
		mInputSeed = container.findComponent("seed-value", TextField);
		mInputSeed.restrictChars = "0-9";
		mInputSeed.registerEvent(UIEvent.CHANGE, function(aEvent:UIEvent) {
			mModel.seed = Std.parseInt(mInputSeed.value);
		});
		
		mInputStarType = container.findComponent("prob-list", ListView);
		for (type in Type.allEnums(EStarType))
			mInputStarType.dataSource.add( { type:type, value:'Класс звезды $type' } );
		mInputStarType.registerEvent(UIEvent.CHANGE, function(aEvent:UIEvent) {
			var item = mInputStarType.selectedItem;
			mStarType = item != null ? item.data.type : null;
			updateProbValue();
		});
		
		mInputStarValue = container.findComponent("prob-value", TextField);
		mInputStarValue.restrictChars = "0-9";
		mInputStarValue.registerEvent(UIEvent.CHANGE, function(aEvent:UIEvent) {
			mModel.probs.probSet(mStarType, Std.parseInt(mInputStarValue.value));
		});
		
		mCheckAuto = container.findComponent("redraw-auto", CheckBox);
		mCheckAuto.registerEvent(UIEvent.CHANGE, function(aEvent:UIEvent) {
			mModel.autoCommit = mCheckAuto.selected;
		});
		
		var buttonRedraw = container.findComponent("redraw", Button);
		buttonRedraw.registerEvent(MouseEvent.CLICK, function(aEvent:MouseEvent) {
			mModel.commit();
		});
	}
	
	function onModel(?aEvent:Event) {
		if (mModel == null)
			return;
		
		mInputStars.value = Std.string(mModel.stars);
		mInputArms.value = Std.string(mModel.arms);
		mInputSpin.value = Std.string(mModel.spin);
		mInputSeed.value = Std.string(mModel.seed);
		updateProbValue();
		mCheckAuto.selected = mModel.autoCommit;
	}
	
	inline function updateProbValue() {
		mInputStarValue.text = mStarType != null ? Std.string(mModel.probs.probGet(mStarType)) : "";
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	var mModel:CSettingsModel;
	var mStarType:EStarType;
	var mInputStars:TextField;
	var mInputArms:TextField;
	var mInputSpin:TextField;
	var mInputSeed:TextField;
	var mInputStarType:ListView;
	var mInputStarValue:TextField;
	var mCheckAuto:CheckBox;
	
}