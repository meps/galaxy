package view;

import model.CGalaxyModel;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.geom.Point;
import openfl.geom.Rectangle;

using utils.CStarUtils;

class CGalaxyView implements ICameraTarget {
	
	public function new() {
		init();
	}
	
	public var container(default, null):Sprite;
	
	public function setup(aModel:CGalaxyModel) {
		mModel = aModel;
		redraw();
	}
	
	public function resize(aWidth:Int, aHeight:Int) {
		mWidth = aWidth;
		mHeight = aHeight;
		redraw();
	}
	
	// ICameraTarget ///////////////////////////////////////////////////////////
	
	public function place(aOffsetX:Float, aOffsetY:Float, aZoom:Float) {
		if (aOffsetX == mOffsetX && aOffsetY == mOffsetY && aZoom == mZoom)
			return;
		
		mOffsetX = aOffsetX;
		mOffsetY = aOffsetY;
		mZoom = aZoom;
		redraw();
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	function init() {
		container = new Sprite();
		mBitmap = new Bitmap();
		container.addChild(mBitmap);
		mFactory = new CStarFactory();
	}
	
	function redraw() {
		if (mModel == null || mWidth == 0 || mHeight == 0)
			return;
		
		prepareRaster();
		mRaster.lock();
		
		var stars = mModel.stars();
		var x0 = (mWidth >> 1) + mOffsetX * SCALE;
		var y0 = (mHeight >> 1) + mOffsetY * SCALE;
		var scale = mZoom * SCALE;
		
		var bounds = new Rectangle();
		var point = new Point();
		for (index in 0...mModel.length) {
			var star = stars[index];
			var starRaster = mFactory.create(star, mZoom);
			if (starRaster == null)
				continue;
			
			bounds.width = starRaster.width;
			bounds.height = starRaster.height;
			point.x = x0 + star.x * scale - bounds.width / 2;
			point.y = y0 - star.y * scale + bounds.height / 2;
			mRaster.copyPixels(starRaster, bounds, point);
		}
		
		mRaster.unlock();
	}
	
	function prepareRaster() {
		if (mRaster == null) {
			mRaster = new BitmapData(mWidth, mHeight, false, 0x000000);
			mBitmap.bitmapData = mRaster;
		} else if (mRaster.width < mWidth || mRaster.height < mHeight) {
			mBitmap.bitmapData = null;
			mRaster.dispose();
			mRaster = new BitmapData(mWidth, mHeight, false, 0x000000);
			mBitmap.bitmapData = mRaster;
		} else {
			mRaster.lock();
			mRaster.fillRect(new Rectangle(0, 0, mWidth, mHeight), 0x000000);
		}
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	var mModel:CGalaxyModel;
	var mWidth:Int;
	var mHeight:Int;
	var mBitmap:Bitmap;
	var mRaster:BitmapData;
	var mFactory:CStarFactory;
	
	var mOffsetX:Float = 0;
	var mOffsetY:Float = 0;
	var mZoom:Float = 1;
	
	static inline var SCALE = 400; // TODO дважды повторяется масштаб
	
}