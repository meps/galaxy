package view;

interface ICameraTarget {
	
	function place(aOffsetX:Float, aOffsetY:Float, aZoom:Float):Void;
	
}