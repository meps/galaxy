package view;

import model.CStar;
import model.CStar.EStarType;
import openfl.display.BitmapData;
import openfl.display.GradientType;
import openfl.display.SpreadMethod;
import openfl.display.Sprite;
import openfl.geom.Matrix;

using utils.CStarUtils;

class CStarFactory {
	
	public function new() {
		mMap = new Map();
	}
	
	public function create(aStar:CStar, aZoom:Float):BitmapData {
		var raster:BitmapData;
		var type = aStar.type;
		var radius = aStar.magnitude * aZoom;
		var index = Std.int(radius);
		radius /= REDUCE;
		if (index == 0)
			// слишком мелкие звезды
			return null;
		
		var mags = mMap.get(type);
		if (mags != null) {
			raster = mags.get(index);
			if (raster != null)
				return raster;
		} else {
			mags = new Map();
			mMap.set(type, mags);
		}
		
		var size = (Std.int(radius) << 1) + 1;
		var buffer = new Sprite();
		var gfx = buffer.graphics;
		var matrix = new Matrix();
		matrix.createGradientBox(radius * 2, radius * 2, 0, 0, 0);
		var color = aStar.color();
		gfx.beginGradientFill(GradientType.RADIAL, [color, color, color, color], [1.0, 1.0, 0.25, 0.0], [0, 85, 127, 255], matrix, SpreadMethod.PAD);
		gfx.drawCircle(radius, radius, radius);
		
		raster = new BitmapData(size, size, true, 0x000000);
		raster.draw(buffer, null, null, null, null, true);
		
		mags.set(index, raster);
		return raster;
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	var mMap:Map<EStarType, Map<Int, BitmapData>>;
	
	static inline var REDUCE = 4; // коэффициент подразбиений размеров звезд
	
}