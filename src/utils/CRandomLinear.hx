package utils;

/** Линейный конгруэнтный генератор псевдослучайной величины */
class CRandomLinear implements IRandom {
	
	public function new(?aSeed:Int) {
		if (aSeed != null)
			reset(aSeed);
	}
	
	// IRandom /////////////////////////////////////////////////////////////////
	
	public function reset(aSeed:Int) {
		mSeed = aSeed;
	}
	
	public function next():Float {
		mSeed = mSeed * 1103515245 + 12345;
		return (mSeed >>> 8) / 0x01000000;
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	var mSeed:Int;
	
}