package utils;

import model.CStar;

class CStarUtils {
	
	/** Цвет звезды в зависимости от ее типа */
	public inline static function color(aStar:CStar):UInt return [0xAABFFF, 0xCAD7FF, 0xFFFFFF, 0xF8F7FF, 0xFFF2A1, 0xFFE46F, 0xFFA040][Type.enumIndex(aStar.type)];
	
	/** Относительный размер звезды в зависимости от типа */
	public inline static function size(aStar:CStar):Int return [15, 7, 3, 2, 1, 1, 1][Type.enumIndex(aStar.type)];
	
}