package utils;

class CRandomUtils {
	
	/** Нормальное распределение по равномерному (преобразование Бокса-Мюллера) */
	public static inline function norm(aRandom:IRandom):Float {
		var x, y, s;
		do {
			x = 2.0 * aRandom.next() - 1.0;
			y = 2.0 * aRandom.next() - 1.0;
			s = x * x + y * y;
		} while (s > 1 || s == 0);
		return x * Math.sqrt(-2 * Math.log(s) / s);
	}
	
	public static inline function cube(aRandom:IRandom):Float {
		var rnd = aRandom.next();
		return rnd * rnd * rnd;
	}
	
	/** Выбрать один из энумов */
	public static inline function select<T:EnumValue>(aRandom:IRandom, aEnum:Enum<T>):T {
		return Type.createEnumIndex(aEnum, Std.int(aRandom.next() * Type.allEnums(aEnum).length));
	}
	
}