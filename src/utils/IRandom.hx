package utils;

interface IRandom {
	
	function reset(aSeed:Int):Void;
	
	function next():Float;
	
}