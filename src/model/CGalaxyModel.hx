package model;

import openfl.events.Event;
import openfl.events.EventDispatcher;

class CGalaxyModel extends EventDispatcher {
	
	function new() {
		super(this);
	}
	
	/** Реальное количество звезд */
	public var length(default, null):Int;
	
	/** Список отобранных по региону звезд */
	public function stars():Array<CStar> {
		// TODO
		
		return mStars;
	}
	
	/** Отобрать звезды по ограничивающему региону */
	public function crop(left:Float, top:Float, right:Float, bottom:Float) {
		// TODO
		
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	function commit() {
		dispatchEvent(new Event(Event.CHANGE));
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	var mStars:Array<CStar>;
	
}
