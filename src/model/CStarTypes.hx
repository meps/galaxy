package model;

import model.CStar.EStarType;
import openfl.events.Event;
import openfl.events.EventDispatcher;
import utils.IRandom;

class CStarTypes extends EventDispatcher {
	
	public function new() {
		super(this);
		mProbs = new Map();
		var count = 0;
		for (type in Type.allEnums(EStarType))
			mProbs.set(type, ++count);
	}
	
	public var data(get, null):Array<EStarType>;
	
	/** Вероятность появления звезды */
	public function probGet(aType:EStarType):Int {
		return aType != null && mProbs.exists(aType) ? mProbs.get(aType) : 0;
	}
	
	/** Задать вероятность появления звезды */
	public function probSet(aType:EStarType, aValue:Int) {
		if (aType == null || probGet(aType) == aValue)
			return;
		
		mIsReady = false;
		mProbs.set(aType, aValue);
		dispatchEvent(new Event(Event.CHANGE));
	}
	
	/** Случайная звезда по заданной плотности распределения */
	public function select(aRandom:IRandom):EStarType {
		if (!mIsReady) {
			// подготовить таблицу матожиданий
			mSumm = 0;
			mExps = new Array();
			for (count in mProbs) {
				mSumm += count;
				mExps.push(mSumm);
			}
			mIsReady = true;
		}
		
		// TODO переделать поиск индекса на метод Ньютона
		var value = Std.int(aRandom.next() * mSumm);
		for (index in 0...mExps.length)
			if (value <= mExps[index])
				return Type.createEnumIndex(EStarType, index);
		return null;
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	function get_data():Array<EStarType> return Type.allEnums(EStarType);
	
	////////////////////////////////////////////////////////////////////////////
	
	var mIsReady:Bool = false;
	var mSumm:Int;
	var mExps:Array<Int>;
	var mProbs:Map<EStarType, Int>;
	
}