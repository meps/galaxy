package model;

import openfl.events.Event;
import openfl.events.EventDispatcher;

class CSettingsModel extends EventDispatcher {
	
	public function new() {
		super(this);
		probs.addEventListener(Event.CHANGE, onProbs);
	}
	
	/** Автоматическое подтверждение на каждое изменение */
	public var autoCommit(default, set):Bool = true;
	
	/** Подтвердить все сделанные изменения */
	public function commit() {
		if (!mDirty)
			return;
		
		mDirty = false;
		dispatchEvent(new Event(Event.CHANGE));
	}
	
	/** Общее количество звезд */
	public var stars(get, set):Int;
	public var starsMin(default, null):Int = 1000;
	public var starsMax(default, null):Int = 100000;
	
	/** Общее количество рукавов */
	public var arms(get, set):Int;
	public var armsMin(default, null):Int = 1;
	public var armsMax(default, null):Int = 10;
	
	/** Сила спина (знаковое, в градусах) */
	public var spin(get, set):Int;
	public var spinMin(default, null):Int = -3600;
	public var spinMax(default, null):Int = 3600;
	
	/** Затравка для генерации */
	public var seed(get, set):Int;
	public var seedMin(default, null):Int = 0x80000000;
	public var seedMax(default, null):Int = 0x7FFFFFFF;
	
	/** Вероятности появления звезд */
	public var probs(default, null):CStarTypes = new CStarTypes();
	
	////////////////////////////////////////////////////////////////////////////
	
	function set_autoCommit(aValue:Bool):Bool {
		if (aValue == autoCommit)
			return aValue;
		
		autoCommit = aValue;
		if (mDirty && autoCommit) {
			mDirty = false;
			dispatchEvent(new Event(Event.CHANGE));
		}
		return aValue;
	}
	
	function get_stars():Int return mStars;
	
	function set_stars(aValue:Int):Int {
		if (aValue > starsMax)
			starsUpdate(starsMax);
		else if (aValue < starsMin)
			starsUpdate(starsMin);
		else
			starsUpdate(aValue);
		return aValue;
	}
	
	function starsUpdate(aValue:Int) {
		if (aValue == mStars)
			return;
		
		mStars = aValue;
		commitData();
	}
	
	function get_arms():Int return mArms;
	
	function set_arms(aValue:Int):Int {
		if (aValue > armsMax)
			armsUpdate(armsMax);
		else if (aValue < armsMin)
			armsUpdate(armsMin);
		else
			armsUpdate(aValue);
		return aValue;
	}
	
	function armsUpdate(aValue:Int) {
		if (aValue == mArms)
			return;
		
		mArms = aValue;
		commitData();
	}
	
	function get_spin():Int return mSpin;
	
	function set_spin(aValue:Int):Int {
		if (aValue > spinMax)
			spinUpdate(spinMax);
		else if (aValue < spinMin)
			spinUpdate(spinMin);
		else
			spinUpdate(aValue);
		return aValue;
	}
	
	function spinUpdate(aValue:Int) {
		if (aValue == mSpin)
			return;
		
		mSpin = aValue;
		commitData();
	}
	
	function get_seed():Int return mSeed;
	
	function set_seed(aValue:Int):Int {
		if (aValue == mSeed)
			return aValue;
		
		mSeed = aValue;
		commitData();
		return aValue;
	}
	
	function onProbs(aEvent:Event):Void {
		commitData();
	}
	
	function commitData() {
		if (autoCommit) {
			mDirty = false;
			dispatchEvent(new Event(Event.CHANGE));
			return;
		}
		
		mDirty = true;
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	var mDirty:Bool = false;
	var mStars:Int = 15000;
	var mArms:Int = 2;
	var mSpin:Int = 600;
	var mSeed:Int = 0x12345678;
	
}