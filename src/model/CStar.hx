package model;

class CStar {
	
	public function new() {
	}
	
	public var x:Float;
	public var y:Float;
	
	/** Спектральный класс звезды */
	public var type:EStarType;
	
	/** Относительная яркость звезды */
	public var magnitude:Int;
	
}

enum EStarType {
	O;
	B;
	A;
	F;
	G;
	K;
	M;
}