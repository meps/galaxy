package model;

import model.CStar.EStarType;
import utils.IRandom;

using utils.CRandomUtils;

class CGalaxyBuilder extends CGalaxyModel{
	
	public static function create():CGalaxyModel return new CGalaxyModel();
	
	public static function update(aTarget:CGalaxyModel, aSettings:CSettingsModel, aRandom:IRandom):CGalaxyModel {
		var result = aTarget != null ? aTarget : create();
		
		aRandom.reset(aSettings.seed);
		
		// не пересоздавать массив, а только переопределять данные
		var stars = result.mStars;
		if (stars == null) {
			stars = new Array();
			result.mStars = stars;
		}
		var starsTotal = aSettings.stars;
		var starIndex = 0; // текущая звезда
		var starLength = stars.length; // общее количество уже имеющихся в модели звезд
		var starTypes = aSettings.probs;
		
		var armsTotal = aSettings.arms;
		var angleStep = Math.PI * 2 / armsTotal; // шаг между отдельными рукавами
		var angleStart = aRandom.next() * angleStep; // начальный угол первого рукава
		var angleSpin = aSettings.spin * Math.PI / 360; // сила закручивания
		
		// создавать звезды поочередно, чтобы сохранять стабильную
		// последовательность их расположения при изменении количества
		result.length = starsTotal;
		while (true) {
			var star:CStar;
			
			// создать звезду в ядре галактики
			if (starsTotal <= 0)
				break;
			--starsTotal;
			
			if (starIndex < starLength) {
				star = stars[starIndex];
			} else {
				star = new CStar();
				stars[starIndex] = star;
			}
			++starIndex;
			
			var distance = aRandom.norm() * CORE_SIZE;
			var angle = aRandom.next() * Math.PI * 2;
			star.x = Math.cos(angle) * distance;
			star.y = Math.sin(angle) * distance;
			star.type = starTypes.select(aRandom);
			star.magnitude = Std.int(aRandom.cube() * MAGNITUDE + 1);
			
			// создать звезду в рукавах галактики
			if (starsTotal <= 0)
				break;
			--starsTotal;
			
			if (starIndex < starLength) {
				star = stars[starIndex];
			} else {
				star = new CStar();
				stars[starIndex] = star;
			}
			++starIndex;
			
			var distance = aRandom.next();
			var angle = angleStart +
				angleStep * Math.floor(aRandom.next() * armsTotal) + // ось рукава
				distance * angleSpin + // закручивание с расстоянием от центра
				angleStep * (ANGLE_BEGIN + distance * (ANGLE_END - ANGLE_BEGIN)) * aRandom.norm(); // разброс внутри рукава
			distance = DIST_BEGIN + distance * (DIST_END - DIST_BEGIN); // привести расстояние к интервалу
			star.x = Math.cos(angle) * distance;
			star.y = Math.sin(angle) * distance;
			star.type = starTypes.select(aRandom);
			star.magnitude = Std.int(aRandom.cube() * MAGNITUDE);
		}
		
		result.commit();
		return result;
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	static inline var CORE_SIZE = 0.3; // размер ядра галактики
	static inline var DIST_BEGIN = 0.1; // начальная дистанция рукава
	static inline var DIST_END = 1.0; // конечная дистанция рукава
	static inline var ANGLE_BEGIN = 0.1; // относительный разброс угла в начале рукава
	static inline var ANGLE_END = 0.2; // относительный разброс угла в конце рукава
	static inline var MAGNITUDE = 10;
	
}