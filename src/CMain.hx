package;

import haxe.Timer;
import haxe.ui.Toolkit;
import haxe.ui.core.Screen;
import model.*;
import openfl.display.FPS;
import openfl.display.Sprite;
import openfl.display.StageAlign;
import openfl.display.StageScaleMode;
import openfl.events.Event;
import utils.*;
import view.*;

using model.CGalaxyBuilder;

class CMain extends Sprite {
	
	public function new() {
		super();
		
		if (stage != null)
			onInit();
		else
			addEventListener(Event.ADDED_TO_STAGE, onInit);
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	function onInit(?aEvent:Event) {
		if (aEvent != null)
			removeEventListener(Event.ADDED_TO_STAGE, onInit);
		
		stage.align = StageAlign.TOP_LEFT;
		stage.scaleMode = StageScaleMode.NO_SCALE;
		
		stage.addEventListener(Event.RESIZE, onResize);
		stage.addEventListener(Event.ENTER_FRAME, onFrame);
		
		Toolkit.init();
		
		mGalaxyView = new CGalaxyView();
		addChild(mGalaxyView.container);
		
		mSettingsView = new CSettingsView();
		Screen.instance.addComponent(mSettingsView.container);
		
		mCamera = new CCamera();
		mCamera.listen(mGalaxyView.container);
		mCamera.append(mGalaxyView);
		
		mRandom = new CRandomLinear();
		
		mSettingsModel = new CSettingsModel();
		mSettingsModel.addEventListener(Event.CHANGE, onSettings);
		mSettingsView.bind(mSettingsModel);
		
		mGalaxyModel = CGalaxyBuilder.create();
		mGalaxyModel.addEventListener(Event.CHANGE, onGalaxy);
		mGalaxyModel.update(mSettingsModel, mRandom);
		
		var fps = new FPS(0, 0, 0xFFFFFF);
		addChild(fps);
		
		onResize();
		onSettings();
	}
	
	function onResize(?aEvent:Event) {
		mStageWidth = stage.stageWidth;
		mStageHeight = stage.stageHeight;
		mUpdateResize = true;
	}
	
	function onFrame(aEvent:Event) {
		var time = Timer.stamp() - mLastTime;
		mLastTime += time;
		
		if (mUpdateResize) {
			mUpdateResize = false;
			mGalaxyView.resize(mStageWidth, mStageHeight);
			mSettingsView.resize(mStageWidth, mStageHeight);
		}
		mCamera.update(time);
	}
	
	function onSettings(?aEvent:Event) {
		mGalaxyModel.update(mSettingsModel, mRandom);
	}
	
	function onGalaxy(aEvent:Event) {
		mGalaxyView.setup(mGalaxyModel);
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	var mLastTime:Float = 0;
	var mUpdateResize:Bool = false; // не рескейлить чаще fps
	var mStageWidth:Int;
	var mStageHeight:Int;
	
	var mRandom:IRandom;
	var mGalaxyModel:CGalaxyModel;
	var mGalaxyView:CGalaxyView;
	var mSettingsModel:CSettingsModel;
	var mSettingsView:CSettingsView;
	var mCamera:CCamera;
	
	
}
